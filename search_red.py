from tinytag import TinyTag
import requests
import config
import json
import sys
import os


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


done = {}
to_up = ['flac', 'wav']
session = None


def connect():
    url = config.URL
    user = config.USER
    password = config.PASSWORD

    data = {'username': user, 'password': password, 'keeplogged': 1,
            'login': 'Log in'}

    global session
    session = requests.session()
    session.post(url, data=data)  # don't bother with source, just connect


def get_artist_id(artist):
    url = "https://redacted.ch/artist.php?action=autocomplete&query={}"
    url = url.format(artist)

    src = session.get(url)
    src = json.loads(src.text)

    if len(src['suggestions']) > 0:
        return src['suggestions'][0]['data']

    return None


def not_found(artist, album, ext, bitrate):
    if ext not in to_up:
        return

    artist = artist.title()
    album = album.title()
    t1 = "{}{} — {}{} not found on website, feel free to upload!"
    t1 = t1.format(bcolors.OKGREEN, artist, album, bcolors.ENDC)

    t2 = '  You have {}"{}"{}'
    t2 = t2.format(bcolors.OKGREEN, ext, bcolors.ENDC)
    print(t1)
    print(t2)
    print('')


def print_result(artist, album, year_type, quality):
    print('Best match: ')
    print('  ', artist, '—', album, '—', year_type)
    print('Best available quality:')
    print('  ', quality)
    print('')


def search_torrent(artist, album, display=True):
    url = "https://redacted.ch/torrents.php?searchstr={}+{}"
    url = url.format(artist, album)

    src = session.get(url).text

    if '<h2>Your search did not match anything.</h2>' in src:
        return False

    artist = src.split('<a href="artist.php?')[1].split('dir="ltr">')[1]
    artist = artist.split('</a>')[0]

    album = src.split(' class="tooltip" title="View torrent group" dir="ltr">')
    album = album[1].split('</a>')

    year_type = album[1].split('<span')[0].strip()
    album = album[0]

    quality = src.split('href="torrents.php?id=')[2].split('">')[1]
    quality = quality.split('</a>')[0].strip()
    quality = quality.split('/ <strong class=')[0].strip()

    if display:
        print_result(artist, album, year_type, quality)

    return True


def recurse(path):
    extensions = ('ogg', 'mp3', 'flac', 'opus', 'm4a', 'aac', 'wma', 'wav')
    for element in os.listdir(path):
        full = os.path.join(path, element)
        if os.path.isdir(full):
            recurse(full)
        else:
            if element.lower().endswith(extensions):
                try:
                    tags = TinyTag.get(full)
                except Exception:
                    continue

                artist, album = tags.albumartist, tags.album
                if not artist:
                    artist = tags.artist

                if not all((artist, album)):
                    continue

                artist = artist.lower()
                album = album.lower()

                if artist not in done.keys():
                    done[artist] = [album]
                else:
                    if album in done[artist]:
                        continue
                    else:
                        done[artist].append(album)

                if not search_torrent(artist, album, display=True):
                    ext = element.split('.')[-1]
                    not_found(artist, album, ext, tags.bitrate)


def usage():
    print('Usage:')
    print('  python3 sarch_red.py <dir>')
    exit()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        usage()

    search_dir = sys.argv[1]
    connect()

    recurse(search_dir)
